import React from 'react';
import logo from './logo.svg';
import './App.css';
import ReactMapboxGl, { Layer, Feature, ZoomControl, ScaleControl, RotationControl } from 'react-mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';

const Map = ReactMapboxGl({
  accessToken:
    'pk.eyJ1IjoiYWJoaWplZXRnb2VsNzciLCJhIjoiY2l3YTZ5dGhhMDRmOTJ6bzVrY3U0aTA2YiJ9.fEbh9TZcBanzrrMChQilJA'
});

class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      lng: 78.342674,
      lat: 29.607981,
      zoom: [12]
    }
  }

  componentDidMount(){
    /*const { lng, lat, zoom } = this.state;
    const map = new mapboxgl.Map({
      container: this.mapContainer,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [lng, lat],
      zoom: zoom
    });

    map.on('move', () => {
      this.setState({
        lng: map.getCenter().lng.toFixed(4),
        lat: map.getCenter().lat.toFixed(4),
        zoom: map.getZoom().toFixed(2)
      });
    });*/
  }

  render(){
    const { lng, lat, zoom } = this.state;
    return (
      <div>
      <Map
        style="mapbox://styles/abhijeetgoel77/ckhyvp2qm0xtg19qqhol47osh"
        containerStyle={{
          height: '100vh',
          width: '100vw'
        }}
        center={[lng, lat]}
        zoom={zoom}
        >
          <Layer type="symbol" id="marker" layout={{ 'icon-image': 'marker-15' }}>
            <Feature coordinates={[-0.481747846041145, 51.3233379650232]} />
          </Layer>
          <ZoomControl />
          <ScaleControl />
          <RotationControl />
        </Map>
      </div>
    );
  }
}

export default App;
